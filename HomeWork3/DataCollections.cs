﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3
{
    public class DataCollections
    {
        public List<User> GetUsers()
        {
            List<User> users = new List<User>()
            {
                 new User{Id                 = 10,
                         Name               ="Alexander",
                         MiddleName         ="Nikolaevich",
                         Surname            = "Petrov",
                         TelephoneNumber    = "89283227892",
                         PassportData       = "789654",
                         RegistrationDate   = new DateTime(1992, 5, 12),
                         Login              ="Alexander1992",
                         Password           = 123321
                },
                new User{Id                 = 15,
                         Name               ="Nikolay",
                         MiddleName         ="Alexandrovich",
                         Surname            = "Ivanov",
                         TelephoneNumber    = "89286321682",
                         PassportData       = "798269",
                         RegistrationDate   = new DateTime(1997, 4, 18),
                         Login              ="Nikolay1997",
                         Password           = 456789
                },
                new User{Id                 = 20,
                         Name               ="Vladimir",
                         MiddleName         ="Petrovich",
                         Surname            = "Krasnov",
                         TelephoneNumber    = "89289638547",
                         PassportData       = "628736",
                         RegistrationDate   = new DateTime(2000, 7, 22),
                         Login              ="Vladimir2000",
                         Password           = 974445
                },
                new User{Id                 = 25,
                         Name               ="Kirill",
                         MiddleName         ="Andreevich",
                         Surname            = "Lazarev",
                         TelephoneNumber    = "89284587214",
                         PassportData       = "482497",
                         RegistrationDate   = new DateTime(2011, 1, 12),
                         Login              ="Kirill2011",
                         Password           = 963258
                },
                new User{Id                 = 30,
                         Name               ="Andrey",
                         MiddleName         ="Andreevich",
                         Surname            = "Voluev",
                         TelephoneNumber    = "89286821997",
                         PassportData       = "796413",
                         RegistrationDate   = new DateTime(2007, 12, 19),
                         Login              ="Andrey2007",
                         Password           = 111244
                },
                new User{Id                 = 35,
                         Name               ="Viktor",
                         MiddleName         ="Viktorovich",
                         Surname            = "Sidorov",
                         TelephoneNumber    = "89284457997",
                         PassportData       = "465564",
                         RegistrationDate   = new DateTime(1995, 1, 2),
                         Login              ="Viktor1995",
                         Password           = 789123
                },
                new User{Id                 = 40,
                         Name               ="Maksim",
                         MiddleName         ="Sergeevich",
                         Surname            = "Sergeev",
                         TelephoneNumber    = "89286658978",
                         PassportData       = "688751",
                         RegistrationDate   = new DateTime(1997, 7, 17),
                         Login              ="Maksim1997",
                         Password           = 428784
                },
                new User{Id                 = 45,
                         Name               ="Sergey",
                         MiddleName         ="Maksimovich",
                         Surname            = "Maksimov",
                         TelephoneNumber    = "89284568574",
                         PassportData       = "681697",
                         RegistrationDate   = new DateTime(2009, 9, 20),
                         Login              ="Sergey2009",
                         Password           = 715894
                },
                new User{Id                 = 50,
                         Name               ="Vladislav",
                         MiddleName         ="Victorovich",
                         Surname            = "Kozlov",
                         TelephoneNumber    = "89284729865",
                         PassportData       = "753357",
                         RegistrationDate   = new DateTime(1994, 3, 3),
                         Login              ="Vladislav1994",
                         Password           = 825258
                },
                new User{Id                 = 55,
                         Name               ="Oleg",
                         MiddleName         ="Olegovich",
                         Surname            = "Rechkin",
                         TelephoneNumber    = "89287788871",
                         PassportData       = "494757",
                         RegistrationDate   = new DateTime(1990, 3, 3),
                         Login              ="Oleg1990",
                         Password           = 998754
                },
            };
            return users;
        }
        public List<Account> GetAccounts()
        {
            List<Account> accounts = new List<Account>()
            {
                new Account{Id = 1,  AmountMoney = 1500,   DateCreateAccount = new DateTime(1992, 7, 22),  IdUser = 55 },
                new Account{Id = 2,  AmountMoney = 1400,   DateCreateAccount = new DateTime(2000, 1, 21),  IdUser = 10},
                new Account{Id = 3,  AmountMoney = 1100,   DateCreateAccount = new DateTime(1954, 4, 12),  IdUser = 15 },
                new Account{Id = 4,  AmountMoney = 12000,  DateCreateAccount = new DateTime(1987, 7, 26),  IdUser = 25 },
                new Account{Id = 5,  AmountMoney = 12345,  DateCreateAccount = new DateTime(1956, 4, 11),  IdUser = 45 },
                new Account{Id = 6,  AmountMoney = 54325,  DateCreateAccount = new DateTime(1945, 9, 5),   IdUser = 35 },
                new Account{Id = 7,  AmountMoney = 324654, DateCreateAccount = new DateTime(1998, 12, 23), IdUser = 25 },
                new Account{Id = 8,  AmountMoney = 64523,  DateCreateAccount = new DateTime(1933, 11, 12), IdUser = 15 },
                new Account{Id = 9,  AmountMoney = 12355,  DateCreateAccount = new DateTime(1978, 1, 15),  IdUser = 40 },
                new Account{Id = 10, AmountMoney = 57632,  DateCreateAccount = new DateTime(1998, 2, 14),  IdUser = 45 },
                new Account{Id = 11, AmountMoney = 232322, DateCreateAccount = new DateTime(1978, 5, 13),  IdUser = 20 },
                new Account{Id = 12, AmountMoney = 675653, DateCreateAccount = new DateTime(1963, 4, 17),  IdUser = 55 },
                new Account{Id = 13, AmountMoney = 23524,  DateCreateAccount = new DateTime(1975, 7, 21),  IdUser = 10 },
            };
            return accounts;
        }
        public List<History> GetHistories()
        {
            List<History> histories = new List<History>()
            {
                new History(){Id = 111,
                              IdAccount = 12,
                              DateOperation = new DateTime(1999, 3, 12),
                              Operation = TypeOperations.WithdrawalFromAccount,
                              Sum = 150
                },
                new History(){Id = 112,
                              IdAccount = 3,
                              DateOperation = new DateTime(1987, 2, 22),
                              Operation = TypeOperations.ReplenishmentOfAAccount,
                              Sum = 300
                },
                new History(){Id = 113,
                              IdAccount = 11,
                              DateOperation = new DateTime(1970, 3, 12),
                              Operation = TypeOperations.WithdrawalFromAccount,
                              Sum = 400
                },
                new History(){Id = 114,
                              IdAccount = 2,
                              DateOperation = new DateTime(1998, 6, 5),
                              Operation = TypeOperations.ReplenishmentOfAAccount,
                              Sum = 200
                },
                new History(){Id = 115,
                              IdAccount = 7,
                              DateOperation = new DateTime(1979, 3, 23),
                              Operation = TypeOperations.ReplenishmentOfAAccount,
                              Sum = 500
                },
                new History(){Id = 116,
                              IdAccount = 5,
                              DateOperation = new DateTime(1983, 8, 22),
                              Operation = TypeOperations.WithdrawalFromAccount,
                              Sum = 340
                },
                new History(){Id = 117,
                              IdAccount = 3,
                              DateOperation = new DateTime(1979, 11, 21),
                              Operation = TypeOperations.ReplenishmentOfAAccount,
                              Sum = 777
                },
                new History(){Id = 118,
                              IdAccount = 9,
                              DateOperation = new DateTime(1985, 10, 11),
                              Operation = TypeOperations.WithdrawalFromAccount,
                              Sum = 543
                },
                new History(){Id = 119,
                              IdAccount = 10,
                              DateOperation = new DateTime(1988, 4, 19),
                              Operation = TypeOperations.ReplenishmentOfAAccount,
                              Sum = 344
                },
                new History(){Id = 120,
                              IdAccount = 6,
                              DateOperation = new DateTime(1999, 7, 24),
                              Operation = TypeOperations.WithdrawalFromAccount,
                              Sum = 223
                }
            };
            return histories;
        }

        public void GetInformation(List<User> users, String login, Int32 password)
        {
            var result = from user in users
                         where user.Login == login && user.Password == password
                         select new { user.Name, user.TelephoneNumber, user.PassportData };

            foreach (var item in result)
                Console.WriteLine($" Имя: {item.Name},\n " +
                    $"Телефон: {item.TelephoneNumber},\n" +
                    $" Паспорт: {item.PassportData}");

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }
        public void GetInformation(List<Account> accounts ,Int32 idUser)
        {
            var result2 = from account in accounts
                          where account.IdUser == idUser
                          select new { account.AmountMoney, account.Id, account.DateCreateAccount };

            foreach (var item in result2)
            {
                Console.WriteLine($" Id:{item.Id},\n" +
                    $" Количество денег: {item.AmountMoney},\n" +
                    $" Дата создания: ({item.DateCreateAccount.ToShortDateString()})");
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }
        public void GetInformation(List<Account> accounts, List<History> histories, Int32 idUser)
        {
            var result3 = from account in accounts
                          where account.IdUser == idUser
                          join history in histories on account.Id equals history.IdAccount
                          select new { account.IdUser, account.AmountMoney, history.DateOperation };


            foreach (var item in result3)
            {
                Console.WriteLine($" id пользователя: {item.IdUser}\n" +
                    $" Денег на счете: {item.AmountMoney},\n" +
                    $" Дата проведения операций со счетом : {item.DateOperation.ToShortDateString()}");
                Console.WriteLine();
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

        }
        public void GetInformation(List<Account> accounts, List<History> histories, TypeOperations operations)
        {
            var result4 = from history in histories
                          where history.Operation == operations
                          join account in accounts on history.IdAccount equals account.Id
                          select new { history.IdAccount, account.IdUser };


            foreach (var item in result4)
            {
                Console.WriteLine($" Id аккаунта,у которого пополнялся счет : {item.IdAccount}\n" +
                                  $" id пользователя, пополнявшего счет: {item.IdUser}");

                Console.WriteLine();
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }
        public void GetInformation(List<Account> accounts, List<User> users, Int32 mountMoney)
        {

            var result5 = from account in accounts
                          where account.AmountMoney > mountMoney
                          join user in users on account.IdUser equals user.Id
                          select new { user.Name, user.Surname };

            foreach (var item in result5)
            {
                Console.WriteLine($" Имя владельца большой суммы денег: {item.Name}\n" +
                                  $" Его же фамилия: {item.Surname} :)");
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }

    }
}
