﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3
{
    class Program
    {
        static void Main(string[] args)
        {
            DataCollections dataCollection = new DataCollections();

            List<User> users = dataCollection.GetUsers();

            List<Account> accounts = dataCollection.GetAccounts();

            List<History> histories = dataCollection.GetHistories();
           

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Console.WriteLine("1.Вывод информации о заданном аккаунте по логину и паролю(Login == Kirill2011, user.Password == 963258)");
            dataCollection.GetInformation(users, "Kirill2011", 963258);

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Console.WriteLine("2.Вывод данных о всех счетах заданного пользователя (IdUser = 55)");
            dataCollection.GetInformation(accounts, 55);


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Console.WriteLine("3.Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту(IdUser = 35)");
            dataCollection.GetInformation(accounts, histories, 35);


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Console.WriteLine("4.Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта");
            dataCollection.GetInformation(accounts, histories, TypeOperations.ReplenishmentOfAAccount);


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Console.WriteLine("Вывод данных о всех пользователях у которых на счёте сумма больше N -100000 (N задаётся из вне и может быть любой)");
            dataCollection.GetInformation(accounts, users, 100000);
           
            Console.ReadKey();
        }
    }
}
