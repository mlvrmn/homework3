﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3
{
    /*Id, дата открытия счёта, сумма на счёте, Id владельца*/
    public class Account
    {
        public Int32 Id                     { get; set; }
        public DateTime DateCreateAccount   { get; set; }
        public Int32 AmountMoney            { get; set; }
        public Int32 IdUser                 { get; set; }
    }
}
