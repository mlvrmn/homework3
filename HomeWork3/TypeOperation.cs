﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3
{
    /* Тип операции*/
    public enum TypeOperations : Int32
    {
        ReplenishmentOfAAccount = 0x00,
        WithdrawalFromAccount = 0x01
    }
}
