﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3
{
    /*Id, Имя, фамилия, отчество, телефон, данные паспорта, дата регистрации, логин, пароль*/
    public class User
    {
        public Int32 Id                     { get; set; }
        public String Name                  { get; set; }
        public String Surname               { get; set; }
        public String MiddleName            { get; set; }
        public String TelephoneNumber       { get; set; }
        public String PassportData          { get; set; }
        public DateTime RegistrationDate    { get; set; }
        public String Login                 { get; set; }
        public Int32 Password               { get; set; }
    }
}
