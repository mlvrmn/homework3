﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3
{
    /*Id, дата операции, тип операции, сумма, Id счёта*/
    public class History
    {
        public Int32 Id                     { get; set; }
        public Int32 IdAccount              { get; set; }
        public Int32 Sum                    { get; set; }
        public DateTime DateOperation       { get; set; }
        public TypeOperations Operation     { get; set; }
    }
}
